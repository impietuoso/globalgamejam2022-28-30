using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AreaOfEffect : MonoBehaviour
{
    [SerializeField]
    UnityEvent OnTriggerEnter;
    [SerializeField]
    UnityEvent OnTriggerExit;

    private void OnTriggerEnter2D(Collider2D col) {
        if (col.tag == "Player")
            OnTriggerEnter.Invoke();
    }

    private void OnTriggerExit2D(Collider2D col) {
        if (col.tag == "Player")
            OnTriggerExit.Invoke();
    }

}
