using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleObject : MonoBehaviour
{
    public int keyAtempt;
    private void OnTriggerEnter2D(Collider2D col) {
        if (col.tag == "HitboxPlayer") {
            transform.Rotate(new Vector3(0,0,-90));
            if (PuzzleManager.instance.key[keyAtempt] < 3) PuzzleManager.instance.key[keyAtempt]++;
            else PuzzleManager.instance.key[keyAtempt] = 0;
            PuzzleManager.instance.AtempClearPuzzle();
        }
    }
}
