using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    public static PuzzleManager instance;
    public GameObject button1;
    public GameObject button2;
    public GameObject tipText;

    public int[] key;

    public Rigidbody2D obstacle;
    private void Awake() {
        instance = this;
    }

    public void ShowNewButton() {
        button1.SetActive(false);
        button2.SetActive(true);
    }

    public void ShowTipText() {
        tipText.SetActive(true);
    }

    public void CleanPuzzle() {
        Debug.Log("Clear Puzzle");
        obstacle.bodyType = RigidbodyType2D.Dynamic;
    }

    public void AtempClearPuzzle() {
        if(key[0] == 0 && key[1] == 2 && key[2] == 1 && key[3] == 3) {
            CleanPuzzle();
        }
    }

    public void NoButtons() {
        button1.SetActive(false);
        button2.SetActive(false);
    }
}
