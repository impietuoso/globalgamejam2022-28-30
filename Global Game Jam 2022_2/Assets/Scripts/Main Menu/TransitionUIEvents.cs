using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionUIEvents : MonoBehaviour {
    
    [SerializeField]UnityEngine.Events.UnityEvent OnEndFadeOut;

    public void CallFadeOutEvent(){
        OnEndFadeOut?.Invoke();
    }

}
