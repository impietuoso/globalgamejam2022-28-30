using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {
    [SerializeField]
    MainPlayerActions.PlayerActionsScript.PlayerActions action; // Inputs do Player

    Rigidbody2D rb; // Corpo Fisico do player
    Animator anim;
    public Collider2D Col; // Colisor do Player

    public BoxCollider2D Hitbox1;
    public BoxCollider2D Hitbox2;

    [SerializeField]
    float speed = 8; // Velocidade do player
    [SerializeField]
    float jumpForce = 5; // Velocidade do player

    [SerializeField]
    bool canAttack = true;

    [SerializeField]
    bool isGrounded;
    bool isRunning;
    bool canMove = true;
    bool canTakeDamage = true;
    bool canJump = true;
    bool isdead;

    [SerializeField]
    LayerMask groundLayer;

    [SerializeField]
    PhysicsMaterial2D idleMaterial;

    [SerializeField]
    PhysicsMaterial2D runMaterial;

    PhysicsMaterial2D currentMaterial;

    Collider2D[] playerColisions;

    Action command;

    public int maxLife;
    public int currentLife;

    public Image[] lives;
    int damageCounter = 0;
    public Vector3 spawPoint;

    //Ataque
    public LayerMask enemyLayer, puzzleLayer;
    public Transform atkPoint1 , atkPoint2;
    public float atkRange1 = 2f;
    public float atkRange2 = 2f;

    void Awake() {
        action = new MainPlayerActions.PlayerActionsScript().Player;
        action.Enable();
        currentLife = maxLife;
        damageCounter = 0;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        action.Jump.performed += _ => Jump();
        action.Attack.performed += _ => command();
        command = Attack1;
        action.Reset.performed += _ => ResetPosition();
        playerColisions = new Collider2D[1];

    }

    void Update() {

        isGrounded = Col.IsTouchingLayers(groundLayer);

        anim.SetBool("isGrounded", isGrounded);
        anim.SetBool("isRunning", isRunning);
        anim.SetFloat("JumpSpeed", rb.velocity.y);

        if (canMove) rb.velocity = new Vector2(action.Run.ReadValue<float>() * speed, rb.velocity.y);

        if (action.Run.ReadValue<float>() == 0 && currentMaterial != idleMaterial && isGrounded) {
            currentMaterial = idleMaterial;
            rb.sharedMaterial = currentMaterial;
        } else if (action.Run.ReadValue<float>() != 0 && currentMaterial != runMaterial) {
            currentMaterial = runMaterial;
            rb.sharedMaterial = currentMaterial;
        }

        if (isGrounded && action.Run.ReadValue<float>() != 0 && !isRunning) {
            isRunning = true;
        }

        if (isRunning && action.Run.ReadValue<float>() == 0 && isGrounded) {
            isRunning = false;
        }

        if (canAttack) {
            if (rb.velocity.x > 0 && transform.localScale.x == -1) {
                transform.localScale = new Vector3(1, 1, 1);
            } else if (rb.velocity.x < 0 && transform.localScale.x == 1) {
                transform.localScale = new Vector3(-1, 1, 1);
            }
        }

    }
    public void OnDrawGizmos ()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(spawPoint, 1);
    }
    public void ResetPosition()
    {

        transform.position = spawPoint;       
        
    }
    public void Attack1() {
        if (canAttack && isGrounded) {
            canAttack = false;
            canMove = false;
            canJump = false;
            anim.SetTrigger("Atk1");
            command = Attack2;
            rb.velocity = new Vector2(0, 0);
            currentMaterial = idleMaterial;
            Invoke("CanAttackAgain", 0.5f);
            
        }
    }

    public void Attack2() {
        if (canAttack && isGrounded) {
            canAttack = false;
            canMove = false;
            canJump = false;
            anim.SetTrigger("Atk2");
            rb.velocity = new Vector2(0,0);
            currentMaterial = idleMaterial;
            Invoke("CanAttackAgain", 0.5f);
            command = Attack1;
        }
    }

    public void Jump() {
        if (isGrounded && canJump) {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(new Vector2(0, jumpForce * 100));
            AudioManager.instance.PlaySound(sfxID.Jump);
        }
    }

    void ResetCombo() {
        anim.Play("Idle");
    }

    private void OnCollisionEnter2D(Collision2D col) {
        if(col.gameObject.tag == "Ground") {
            AudioManager.instance.PlaySound(sfxID.Landing);
            if (isdead) Death();
        } else if (col.gameObject.tag == "Spike" && canTakeDamage) {
            TakeDamage();
        }
    }

    private void OnTriggerEnter2D(Collider2D col) {
        if(col.tag == "Spike" && canTakeDamage) {
            TakeDamage();
        }
    }

    public void StepSound() {
        Debug.Log("Step Sound");
        AudioManager.instance.PlaySound(sfxID.Footsteps1);
    }

    void CanAttackAgain() {
        canAttack = true;
        canMove = true;
        canJump = true;
    }

    public void SwordSwingSFX() {
        AudioManager.instance.PlaySound(sfxID.Swing);
    }

    public void SwordImpactSFX() {
        AudioManager.instance.PlaySound(sfxID.Impact);
    }

    public void ShowHitBox1() {
        Hitbox1.enabled = false;
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(atkPoint1.position, atkRange1, enemyLayer);
        foreach(Collider2D enemy in hitEnemies)
        {
            enemy.GetComponent<EnemyActions>().TakeDamage();
        }
        
    }

    
    public void ShowHitBox2() {
        Hitbox2.enabled = false;
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(atkPoint2.position, atkRange2, enemyLayer);
        foreach (Collider2D enemy in hitEnemies)
        {
            enemy.GetComponent<EnemyActions>().TakeDamage();
        }
       
    }

    public void TakeDamage() {
        StartCoroutine(DamageTaking());
    }

    IEnumerator DamageTaking() {
        canTakeDamage = false;
        currentLife--;
        lives[damageCounter].gameObject.SetActive(false);
        damageCounter++;
        canMove = false;
        canAttack = false;
        canJump = false;
        rb.AddForce(new Vector2(jumpForce * 50 * -transform.localScale.x, jumpForce * 100));
        AudioManager.instance.PlaySound(sfxID.Hurt);
        anim.SetTrigger("TakeHit");
        yield return new WaitForSeconds(1f);
        canMove = true;
        canAttack = true;
        canJump = true;
        yield return new WaitForSeconds(1f);
        canTakeDamage = true;
    }

    public void CheckDeath() {
        if (currentLife <= 0) {
            isdead = true;
        }
    }

    public void Death() {
        if(currentLife <= 0) {
            anim.SetTrigger("Death");
            AudioManager.instance.PlaySound(sfxID.Death);
            canAttack = false;
            canMove = false;
            canJump = false;
            rb.bodyType = RigidbodyType2D.Kinematic;
            rb.velocity = Vector2.zero;
            Invoke("EndPlayer", 2f);
        }
    }

    void EndPlayer() {
        SceneTransitions.instance.GoToScene(1);
        Destroy(this);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(atkPoint1.position, atkRange1);
        Gizmos.DrawWireSphere(atkPoint2.position, atkRange2);
    }

    void ShowBoxCol()
    {
        Hitbox1.enabled = true;
    }
    void ShowBoxCol2()
    {
        Hitbox2.enabled = true;
    }
}
