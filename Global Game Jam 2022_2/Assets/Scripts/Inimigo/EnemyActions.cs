using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActions : MonoBehaviour
{

    public bool walk = false;
    public Animator anim;
    public bool attack = false;
    public bool isdead = false;
    public GameObject Hitbox;
    public LayerMask playerLayer;
    public Transform atkPoint;
    public float atkRange = 0.5f;
    

    public int maxLife;
    int currentLife;

    void Start()
    {
        currentLife = maxLife;
        anim.SetBool("walk", true);
        
        

    }

    
    void Update()
    {

               
    

    }

    public void TakeDamage()
    {
        currentLife--;
        anim.SetTrigger("hurt");
        if(currentLife <= 0)
        {
            isdead = true;
            EnemyDeath();
            GetComponent<EnemyMove>().enabled = false;
            GetComponent<SliderJoint2D>().enabled = false;
            Invoke("TrueDeath",2f);

        }
    }

   
    void TrueDeath()
    {
        Destroy(this.gameObject);
    }

    public void ShowHitBox()
    {
        Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(atkPoint.position, atkRange, playerLayer);
        foreach (Collider2D player in hitPlayer)
        {
            player.GetComponent<PlayerMovement>().TakeDamage();
        }
    }

   
    public void EnemyDeath()
    {
        
        anim.SetBool("isdead", true);
        GetComponent<Rigidbody2D>().velocity = Vector3.zero;

    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(atkPoint.position, atkRange);
    }


}
