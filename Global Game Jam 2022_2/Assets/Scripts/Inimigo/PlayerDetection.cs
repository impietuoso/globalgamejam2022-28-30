using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetection : MonoBehaviour
{
    public EnemyActions enemy;
    public bool canDetect;
    [Range(0,2)]
    public float timeUntilNextDetection;

    private void OnTriggerStay2D(Collider2D outro)
    {
        if (canDetect && !enemy.isdead)
        {
            if(outro.tag == "Player")
            {
                canDetect = false;
                StartCoroutine(TimerUntilDetectAgain());
            }
        }
    }

    IEnumerator TimerUntilDetectAgain()
    {
        enemy.anim.SetBool("walk", false);
        enemy.anim.SetTrigger("atk");
        yield return new WaitForSeconds(timeUntilNextDetection);
        canDetect = true;
        enemy.anim.SetBool("walk", true);
    }

    private void OnTriggerEnter2D(Collider2D outro)
    {
        if (outro.tag == "Player")
        {
            GetComponentInParent<EnemyMove>().StopEnemy();
           /* float distancia = outro.gameObject.transform.position.x - enemy.transform.position.x;
            if (distancia <= 0)
            {
                enemy.transform.localScale = new Vector2(-1, 1);
            }
            else
            {
                enemy.transform.localScale = new Vector2(1, 1);
            }*/
        }

        /*if(outro.tag == "HitboxPlayer")
        {
            if (!enemy.isdead)
                enemy.TakeDamage();
        }*/
    }
    private void OnTriggerExit2D(Collider2D outro)
    {
        if (outro.tag == "Player")
        {
            GetComponentInParent<EnemyMove>().enemyState = 0;
        } 
    }
}
