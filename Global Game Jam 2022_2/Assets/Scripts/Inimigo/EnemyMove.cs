using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    public SliderJoint2D path;
    public JointMotor2D temp;
    public int enemyState = 0;
    

    void Start()
    {
        temp = path.motor;
        path.useMotor = true;
        EnemyWalk();
    }


    void Update()
    {
        if (enemyState == 0)
        {
            EnemyWalk();
        }

    }
    public void StopEnemy()
    {
        enemyState = 1;
        path.useMotor = false;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
    }
    public void EnemyWalk()
    {
        path.useMotor = true;

        if (path.limitState == JointLimitState2D.LowerLimit)
        {
            temp.motorSpeed = 2;
            path.motor = temp;
            transform.localScale = new Vector2(-1, 1);
        }
        if (path.limitState == JointLimitState2D.UpperLimit)
        {
            temp.motorSpeed = -2;
            path.motor = temp;
            transform.localScale = new Vector2(1, 1);
        }


    }
    
}
