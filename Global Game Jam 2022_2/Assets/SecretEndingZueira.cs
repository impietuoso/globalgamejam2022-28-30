using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SecretEndingZueira : MonoBehaviour
{
    [SerializeField]
    UnityEvent action;
    private void OnTriggerEnter2D(Collider2D col) {
        if(col.tag == "Player") {
            action.Invoke();
        }
    }
}
